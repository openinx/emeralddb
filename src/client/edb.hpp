#ifndef _EDB_HPP_
#define _EDB_HPP_

#include "core.hpp"
#include "ossSocket.hpp"
#include "commandFactory.hpp"

const int CMD_BUFFER_SIZE = 512 ; 

class Edb{
public:
	Edb(){}
	~Edb(){}
public:
	void start();
protected:
    void prompt(void);
private:
    char* readLine(char *p, int length);
    void split(const std::string &text, char delim, std::vector<std::string> &result);
    int readInput(const char *pPrompt, int numIndent);

private:
    ossSocket       _sock;
    CommandFactory  _cmdFactory;
    char            _cmdBuffer[CMD_BUFFER_SIZE];
};

#endif
