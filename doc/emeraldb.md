msg -- message 
pmd -- process manager dispatcher.
dms -- data manage service
ixm  -- index manage
oss -- operate system service
  1. ossXLatch
  2. ossSLatch
  3. ossMmapFile
  4. ossMmapSegment
  5. ossQueue
  6. ossPrimitiveFileOp
  7. ossSocket

pd  -- problem determination.
krcb --- kernel controle block.
eduMgr --- Engine dispatch Unit --- Thread 
eduMgr --- thread pool.
pmdeducb -- pmd edu control block.
pmdEduEvent -- 
pmdOptions -- start up options.
RTN -- runtime ; 
        1. replication
        2. commit & rollback.
        3. write log & read log

IxmBucketManager -- IxmBucket.
dmsFile --- Data managment service file.
   1. insert
   2. remove
   3. find



## Lecture--DBDev01-3.mp4 
1. Makefile: autotools(./configure)
2. boost -- net, thread, lock. -- cross-platform
3. link-style: static. 
4. support java client

gcc,g++,gdb,vim, eclipse,autotools,boost,bson,gson.X11-apps, Java1.6, log4j

## Lecture--DBDev03-1.mp4

## signal 问题
`signal_handler`里面写日志可能导致的问题。 在写日志的时候，需要获得写文件的锁，这时候假设有其他的线程写日志，那么会导致其他线程写不进日志。所以尽量把`signal_handler`手机的简单。在`signal_handler`里面写memset的时候也是比较危险的。

KRCB --- KeRnel Control Block. 


## Lecture--DBDev06-2.mp4
mmap 文件内存映射，不能从本质上减少IO。只是在逻辑上方便文件的操作方式。让访问文件，就像访问内存一样方便。

客户端每次发送命令组成的数据的字节长度不能超过4096.

## Lecture--DBDev09-1.mp4
1. B-Tree
2. BitMap Index
3. R-Tree
4. Quadtree












